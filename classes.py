# -*- coding: utf-8 -*-
"""Created on Fri Jul 27 18:58:25 2018@author: 簡子琦"""
# import numpy as np

from bisect import *

class Material:
    def __init__(self, id, density, E, A, I_y, I_z, J):
        self.id = id
        self.density = density
        self.E = E
        self.Area = A
        self.I_y = I_y
        self.I_z = I_z
        self.J = J


class KeyifyList(object):
    def __init__(self, list, key_lambda):
        self.list = list
        self.key_lambda = key_lambda

    def __len__(self):
        return len(self.list)

    def __getitem__(self, i):
        return self.key_lambda(self.list[i])

class Assembly:    
    def __init__(self):
        self._node_list = []
        self._element_list = []
        self._eq_number = 0
    
    @property
    def nodes(self):
        return self._node_list
    
    def add_node(self, n):
        if n == None:
            return

        nodes = self.nodes
        id = n.id
        i = bisect_left(KeyifyList(nodes, lambda node: node.id), id)
        if i == len(nodes) or id != nodes[i].id:
            nodes.insert(i, n)
            return

        # print("Two nodes have the same id")
    
    def node(self, id):
        nodes = self.nodes
        i = bisect_left(KeyifyList(nodes, lambda node: node.id), id)
        if i == len(nodes) or id != nodes[i].id:
            return None
        else:
            return nodes[i]
       
    @property
    def elements(self):
        return self._element_list
    
    def add_element(self, e):
        if e == None:
            return

        elements = self.elements
        id = e.id
        i = bisect_left(KeyifyList(elements, lambda el: el.id), id)
        if i == len(elements) or id != elements[i].id:
            elements.insert(i, e)
            return

        # print("Two elements have the same id")
    
    def element(self, id):
        elements = self.elements
        i = bisect_left(KeyifyList(elements, lambda el: el.id), id)
        if i == len(elements) or id != elements[i].id:
            return None
        else:
            return elements[i]    
    
    @property
    def eq_number(self):
        return self._eq_number
    
    def assign_eq_number(self):
        self._eq_number = 0
        
        for n in self._node_list:
            self._eq_number = n.assign_eq_number(self._eq_number)
    

    @property
    def lumped_mass(self):
        mass_list = [0.0] * self._eq_number
        
        for e in self.element_list:
            e_lumped = e.lumped_mass
            for i, dof in enumerate(e.dofs):
                if not dof.is_restrained:
                    mass_list[dof.eq_number] += e_lumped[i]
        return mass_list
    

    @property
    def internal_force(self):
        force_list = [0.0] * self.eq_number

        for e in self.element_list:
            e_force = e.internal_force
            for i, dof in enumerate(e.dofs):
                if not dof.is_restrained:
                    force_list[dof.eq_number] += e_force[i]
        return force_list
        

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        