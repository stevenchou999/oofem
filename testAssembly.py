# AssemblyTest.py is created by Kuang-Wu Chou on September 10, 2018 
import unittest

from classes import *
from Node import *

class AssemblyTest(unittest.TestCase):
    def setUp(self):
        self.assembly = Assembly()
        self.label = [10, 201, 9000102]       

    def test_add_node(self):
        assembly = self.assembly
        nodes = assembly.nodes
        label = self.label

        node0 = Node(label[0], Point(0, 0, 0))
        node1 = Node(label[1], Point(1, 0, 0))
        node2 = Node(label[2], Point(2, 0, 0))

        # add node in and arbitrary order
        assembly.add_node(node2)
        assembly.add_node(node0)
        assembly.add_node(node1)

        find_node = assembly.node
        assertTrue = self.assertTrue

        for i in range(3):
            node = find_node(label[i])
            assertTrue(node != None and node == nodes[i])

    def test_add_element(self):
        assembly = self.assembly
        elements = assembly.elements
        label = self.label
        
        # it is more complex to add an element because of many element types 
        


if __name__ == "__main__":    
    unittest.main()



