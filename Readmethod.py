# -*- coding: utf-8 -*-
"""Created on Sun Aug 19 17:24:15 2018@author: chasel2361"""
from classes import*
from Element import*
from Node import*

class Read:
	
    def __init__(self, assembly): 
        self.assembly = assembly
        self.read_raw()
        self.extract_metadata()  #metadata:描述資料的資料
        self.extract_force_description()
        self.extract_node()
        self.extract_rayleigh_damping()
        self.extract_element()
        self.extract_material()
        self.extract_ground_line()
        self.extract_acceleration_duration()
        self.extract_output_data()
        
        
    def read_raw(self):
        
        f = open('input.dat', 'r')
        self.lines = f.readlines()
        f.close
		
        __lines = self.lines
        while '\n' in __lines:
            __lines.remove('\n')
        
        
    def extract_metadata(self):
        self.metadata = [value for value in self.lines[1].split()]
        self.node_num = int(self.metadata[1])
        self.element_num = int(self.metadata[2])
        
        #讀取特殊元素資料
        self.special_element = [float(value) for value in self.lines[3].split()]
        
 		
		#讀取力輸入基本資料
    def extract_force_description(self):
        self.force_description = [float(value) for value in self.lines[2].split()]

		
		#讀取節點資料
    def extract_node(self):
        _node_list = self.assembly.node_list
        for i in range(self.node_num):
            data = [float(value) for value in self.lines[i+4].split()]
            point = Point(data[1], data[2], data[3])
            self.assembly.add_node(Node(i+1, point))            
        
    
    #讀取阻尼資料
    def extract_rayleigh_damping(self):
        _node_list = self.assembly.node_list
        self.rayleigh_damping = [float(value) for value in self.lines[len(_node_list)+4].split()]
		
    
        #讀取元素資料
    def extract_element(self):
        _node_list = self.assembly.node_list
        _element_list = self.assembly.element_list

        for i in range(self.element_num):
            data = [float(value) for value in self.lines[i + len(_node_list) + 5].split()]
            for n in _node_list:
                if int(data[1]) == n.id:
                    n1 = n
                elif int(data[2]) == n.id:
                    n2 = n
            _element_list.append(Beam2D(int(data[0]), n1, n2, data[4], data[5], data[6]))


        #讀取材料特性
    def extract_material(self):
        _node_list = self.assembly.node_list
        _element_list = self.assembly.element_list
        self.material = []
        for i in range(int(self.metadata[4])):
            self.num = i + int(len(_node_list) + len(_element_list) + 5)
            self.lines[self.num] = self.lines[self.num] + self.lines[self.num + 1]
            del self.lines[self.num + 1]
            data = [float(value) for value in self.lines[self.num].split()]
            self.material.append(Material(data[0], data[1], data[2], data[3], data[4], data[5], data[6]))
        for m in self.material:
            for obj in _element_list:
                if obj.material == m.id:
                    obj.material = m
        
        
    def extract_ground_line(self):
        #不確定是不是地表係數與地面線
        del self.lines[self.num + 2]
        del self.lines[self.num + 3]
        self.lines[self.num + 1] = self.lines[self.num + 1] + self.lines[self.num + 2]
        del self.lines[self.num + 2]
        self.ground_data = [float(value) for value in self.lines[self.num + 1].split()]
        
        
    def extract_acceleration_duration(self):
        self.acc_sets = int(self.lines[self.num + 2].split()[0])
        self.time_num = int(self.lines[self.num + 2].split()[1])
        self.g = float(self.lines[self.num + 3].split()[0])
        
        self.time = []
        self.acc = []
        for i in range(self.time_num):
            data = self.lines[self.num + i + 4].split()
            self.time.append(float(data[0]))
            self.acc.append(float(data[1]))
        self.i = i
        
    
    def extract_output_data(self):
        #輸出
        self.num = self.num + self.i + 6
        data = self.lines[self.num].split()
        self.node_output = data[0]
        self.output_type = data[1]
        self.output_dir = data[2]