# -*- coding: utf-8 -*-
"""Created on Thu Jul 26 17:19:15 2018@author: chasel"""
from classes import*
from Element import*
from Node import*

f = open('input.dat', 'r')
lines = f.readlines()
f.close

while '\n' in lines:
    lines.remove('\n')

#讀取基本資料
basic_data = [value for value in lines[1].split()]
node_num = int(basic_data[1])
element_num = int(basic_data[2])

#讀取力輸入基本資料
basic_force = [float(value) for value in lines[2].split()]

#讀取特殊元素資料
special_element = [float(value) for value in lines[3].split()]

#讀取節點資料
node = []
for i in range(int(node_num)):
    data = [float(value) for value in lines[i+4].split()]
    point = Point(data[1], data[2], data[3])
    node.append(Node(i+1, point))


#讀取阻尼資料
Rayleigh_damping = [float(value) for value in lines[len(node)+4].split()]

SimpleBeam = []
#讀取元素資料
for i in range(element_num):
    data = [float(value) for value in lines[i + len(node) + 5].split()]
    for n in node:
        if data[1] == n.id:
            n1 = n
        elif data[2] == n.id:
            n2 = n
    SimpleBeam.append(Beam2D(data[0], n1, n2, data[4], data[5], data[6]))

#讀取材料特性
material = []
for i in range(int(basic_data[4])):
    num = i + int(len(node)+len(SimpleBeam) + 5)
    lines[num] = lines[num] + lines[num + 1]
    del lines[num + 1]
    data = [float(value) for value in lines[num].split()]
    material.append(Material(data[0], data[1], data[2], data[3], data[4], data[5], data[6]))
for mat in material:
    for obj in SimpleBeam:
        if obj.material == mat.id:
            obj.material = mat

#不確定是不是地表係數與地面線
del lines[num + 2]
del lines[num + 3]
lines[num + 1] = lines[num + 1] + lines[num + 2]
del lines[num + 2]
ground_data = [float(value) for value in lines[num + 1].split()]


acc_sets = int(lines[num + 2].split()[0])
time_num = int(lines[num + 2].split()[1])

g = float(lines[num + 3].split()[0])

time = []
acc = []
for i in range(time_num):
    data = lines[num + i + 4].split()
    time.append(float(data[0]))
    acc.append(float(data[1]))


#輸出
num = num + i + 6
data = lines[num].split()
node_output = data[0]
output_type = data[1]
output_dir = data[2]


#"""嘗試組合質量矩陣"""
#assemble = Assembly()
#for s in SimpleBeam:
#    assemble.add_element(s)
#print(assemble.lump_mass)