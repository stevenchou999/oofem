# -*- coding: utf-8 -*-
"""Created on Mon Aug 20 16:41:01 2018@author: chasel2361"""
from Vector import*
import warnings

class ActiveDof:
    
    def __init__(self):
        self._d = 0.0
        self._d_last = 0.0
        self._d_trail = 0.0
        self._d_trail_last = 0.0
        self._v = 0.0
        self._a = 0.0
        self._eq_number = -1
        
    @property
    def d_trail(self):
        return self.d_trail

    @property
    def d_trail_last(self):
        return self.d_trail_last

    @property
    def d_last(self):
        return 

    @property
    def d(self):
        return self._d

    @d.setter
    def d(self, dis):
        self._d = dis
    
    def inc_d(self, delta_d):
        self.d += delta_d[self.eq_number]

    @property
    def is_restrained(self):
        return False
    

    @property
    def eq_number(self):
        return self._eq_number
    
    @eq_number.setter
    def eq_number(self, eq):
        if eq < 0:
            return warnings.warn('Can\'t set eq_number as an negative number!', UserWarning)
        else:
            self._eq_number = eq
            


    @d.setter
    def d(self, dis):
        self._d = dis

    def inc_d(self, delta_d):
        self._d += delta_d[self.eq_number]
        


class BoundaryDof:    
    @property
    def eq_number(self):
        return -1
    
    
    @property
    def is_restrained(self):
        return True
    
    
    @property
    def d(self):
        return 0

    @d.setter
    def d(self, dis):
        return 0
        


class Node:
    def __init__(self, id , p):
        self._id = id
        self._pos0 = p
        self._pos1 = p
        self._dofs = [ActiveDof() for n in range(6)] 
        #如果用 [ActiveDof()] * 6 的話六個都會是同一個物件
        
    
    @property
    def current_pos(self):
        return self._pos1

    @property
    def last_pos(self):
        return self._pos0
    
    @current_pos.setter
    def current_pos(self, p):
        self._pos0 = self._pos1
        self._pos1 = p
    
    
    @property
    def id(self):
        return self._id
    

    def dof(self, i):
        return self._dofs[i]

    def modify_dof(self, i, dof):
        self._dofs[i] = dof
    
    
    def assign_eq_number(self, eq_number):
        for dof in self._dofs:
            if dof.is_restrained == True:
                continue
            else:
                dof.eq_number = eq_number
                eq_number += 1

        return eq_number
    