# -*- coding: utf-8 -*-
"""Created on Fri Jul 27 18:58:25 2018@author: 簡子琦"""
import numpy as np

class Element:
    def __init__(self, id, node1, node2, material, yielding, damping_type):
        self.id = id
        self.node1 = node1
        self.node2 = node2
        self.material = material
        self.yielding = yielding
        self.dampint_type = damping_type

class simple_beam(Element):
    def __init__(self, id, node1, node2, material, yielding, damping_type):
        self._id = id
        self._node1 = node1
        self._node2 = node2
        self._material = material
    
    @property
    def id(self):
        return self._id
    @property
    def node1(self):
        return self._node1
    @node1.setter
    def node1(self, node1):
        self._node1 = node1
    @property
    def node2(self):
        return self._node2
    @node2.setter
    def node2(self, node2):
        self._node2 = node2
    @property
    def material(self):
        return self._material
    @material.setter
    def material(self, material):
        self._material = material

    """current_r 代表現在元素中兩節點的向量(1 → 2), 儲存成np.array的形式方便之後計算"""
    @property
    def current_r(self):        
        r = np.array([self.node2.current_pos.x - self.node1.current_pos.x,
                      self.node2.current_pos.y - self.node1.current_pos.y,
                      self.node2.current_pos.z - self.node1.current_pos.z])
        return r Vector(node2.current_pos, node1.current_pos)
    
    """last_r 代表上一步時元素兩節點的向量(1 → 2), 儲存成np.array的形式方便之後計算"""
    @property
    def last_r(self):        
        r = np.array([self.node2.last_pos.x - self.node1.last_pos.x,
                      self.node2.last_pos.y - self.node1.last_pos.y,
                      self.node2.last_pos.z - self.node1.last_pos.z])
        return r
    
    @property
    def current_length(self):
        return np.linalg.norm(self.current_r, ord = 2)
    @property
    def last_length(self):
        return np.linalg.norm(self.last_r, ord = 2)
    @property
    def elongation(self):
        elongation = self.current_length - self.last_length
        return elongation
    
    @property
    def internal_force(self):
        force = self.elongation * self.material.E * self.material.A
        return force
    
    """這邊rotation的公式是求在旋轉平面上的旋轉角, 純粹拿來實驗是否能夠計算"""
    @property
    def rotation(self):
        dot_1_2 = np.dot(self.current_r, self.last_r)
        return np.arccos(dot_1_2/self.current_length/self.last_length)

    def lump_mass(self)):
        return []

class Material:
    def __init__(self, id, density, E, A, I_y, I_z, J):
        self.id = id
        self.density = density
        self.E = E
        self.A = A
        self.I_y = I_y
        self.I_z = I_z
        self.J = J

class Assembly:
    
    def __init__(self):
        self._eq_number = 0

    def add_node(self, n):
        self.nodes.append(n)
   
    def add_element(self, e):
        self.elements.append(e)

    def node(self, node_id):
        return (find from self.nodes the node whose node id is node_id)
    
    @property
    def eq_number(self):
        return self._eq_number
    
    # do the following first
    def assign_eq_number(self):
        self._eq_number = 0
        
        for n in self.nodes:
            self._eq_number = n.assign_eq_number(self._eq_number)


    def lump_mass(self):        
        mass_list = [0.0] * self.eq_number
        
        for e in self.elements:
            e_lump = e.lump_mass()
            for i, dof in enumerate(e.dofs):
                mass_list[dof.eq_number] += e_lump[i]
                
        return mass_list