class Point:
    
    def __init__(self, x = 0, y = 0, z = 0):
        self.x = x
        self.y = y
        self.z = z
    
    @property
    def value(self):
        return [self.x, self.y, self.z]

    def __add__(self, pt):
        return Point(self.x + pt.x, self.y + pt.y, self.z + pt.z)
    
    def __iadd__(self, pt):
        self.x += pt.x
        self.y += pt.y
        self.z += pt.z
        return self
    
    def __sub__(self, pt):
        return Point(self.x - pt.x, self.y - pt.y, self.z - pt.z)
    
    def __isub__(self, pt):
        self.x -= pt.x
        self.y -= pt.y
        self.z -= pt.z
        return self
    
    def __mul__(self, constant):
        return Point(self.x * constant, self.y * constant, self.z * constant)
    
    def __imul__(self, constant):
        self.x *= constant
        self.y *= constant
        self.z *= constant
        return self
    
    def __truediv__(self, constant):
        return Point(self.x / constant, self.y / constant, self.z * constant)
    
    def __itruediv__(self, constant):
        self.x /= constant
        self.y /= constant
        self.z /= constant
        return self
    
class Vector:
    
    def __init__(self, p1, p2):
        self.x = p2.x - p1.x
        self.y = p2.y - p1.y
        self.z = p2.z - p1.z
    
    @property
    def value(self):
        return [self.x, self.y, self.z]
    
    @property
    def length(self):
        return (self.x ** 2 + self.y ** 2 + self.z ** 2) ** 0.5
    
    def __add__(self, vector):
        return Point(self.x + vector.x, self.y + vector.y, self.z + vector.z)
    
    def __iadd__(self, vector):
        self.x += vector.x
        self.y += vector.y
        self.z += vector.z
        return self
    
    def __sub__(self, vector):
        return Point(self.x - vector.x, self.y - vector.y, self.z - vector.z)
    
    def __isub__(self, vector):
        self.x -= vector.x
        self.y -= vector.y
        self.z -= vector.z
        return self
    
    def __mul__(self, constant):
        return Point(self.x * constant, self.y * constant, self.z * constant)
    
    def __imul__(self, constant):
        self.x *= constant
        self.y *= constant
        self.z *= constant
        return self
    
    def __truediv__(self, constant):
        return Point(self.x / constant, self.y / constant, self.z * constant)
    
    def __itruediv__(self, constant):
        self.x /= constant
        self.y /= constant
        self.z /= constant
        return self














