# -*- coding: utf-8 -*-
"""Created on Thu Aug 16 13:08:36 2018@author: 簡子琦"""
from Vector import *
import numpy as np

class Beam2D:
    def __init__(self, id, node1, node2, material, yielding, damping_type):
        self._id = id
        
        self._node1 = node1
        self._node2 = node2
        self._dofs = [
                node1.dof(0), node1.dof(1), node1.dof(5),
                node2.dof(0), node2.dof(1), node2.dof(5)]        
        self._material = material
    
    @property
    def id(self):
        return self._id
    
    @property
    def node1(self):
        return self._node1
    
    @node1.setter
    def node1(self, node1):
        self._node1 = node1
    
    
    @property
    def node2(self):
        return self._node2
    
    @node2.setter
    def node2(self, node2):
        self._node2 = node2
    
    @property
    def dofs(self):
        return self._dofs
    
    def dof(self, index):
        return self._dofs[index]
    
    @property
    def material(self):
        return self._material
    
    @material.setter
    def material(self, material):
        self._material = material


    """current_r 代表現在元素中兩節點的向量(1 → 2)"""
    @property
    def current_r(self):
        return Vector(self.node2.current_pos, self.node1.current_pos)
    
    """last_r 代表上一步時元素兩節點的向量(1 → 2)"""
    @property
    def last_r(self):
        return Vector(self.node2.last_pos, self.node1.last_pos)
    
    @property
    def current_length(self):
        return self.current_r.length
    
    @property
    def last_length(self):
        return self.last_r.length
    
    @property
    def elongation(self):
        elongation = self.current_length - self.last_length
        return elongation
    
    """這邊rotation的公式是求在旋轉平面上的旋轉角, 純粹拿來實驗是否能夠計算"""
    @property
    def rotation(self):
        dot_1_2 = np.dot(self.current_r, self.last_r)
        return np.arccos(dot_1_2/self.current_length/self.last_length)
    
    
    @property
    def lumped_mass(self):
        mass = self.current_length * self.material.density / 2
        #質點沒有慣性矩
        inertia = self.material.density * self.current_length/2 * self.material.I_z / self.material.Area
        mass = [mass, mass, inertia, mass, mass, inertia]
        return mass
    
    @property
    def internal_force(self, rotation_a = 0, rotation_b = 0):
        _E = self.material.E
        _A = self.material.Area
        _I = self.material.I_z
        _l = self.current_length
        f_x_b = _E * _A / _l * self.elongation
        f_x_a = -1 * f_x_b
        m_a = 2 * _E * _I / _l * (2 * rotation_a + rotation_b)
        m_b = 2 * _E * _I / _l * (rotation_a + 2 * rotation_b)
        f_y_a = (m_a + m_b) / _l
        f_y_b = -1 * f_y_a
        
        force = [f_x_a, f_y_a, m_a, f_x_b, f_y_b, m_b]
        return force
    
    
    
    
    
    
    
    
    
    
    
    