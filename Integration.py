# -*- coding: utf-8 -*-
"""Created on Mon Aug 20 17:36:03 2018@author: chasel2361"""

class Newmark:
    
    def __init__(self, delta_t, beta = 0.25, gamma = 0.5):
        self._beta = beta
        self._gamma = gamma
        self._delta_t = delta_t  #加上@property
        self._b1 = 1 / (beta * delta_t**2)
        self._b2 = 1 / (beta * delta_t)
        self._b3 = -(1 / (2 * beta) - 1)
        self._b4 = gamma / (beta * delta_t)
        self._b5 = 1 - gamma / beta
        self._b6 = delta_t * (1 - gamma / (2 * beta))
        
    @property
    def parameters(self):
        print('b1 = %.2f\nb2 = %.2f\nb3 = %.2f\nb4 = %.2f\nb5 = %.2f\nb6 = %.5f'
              % (self._b1, self._b2, self._b3, self._b4, self._b5, self._b6))
    
    def update_b1(self, delta_t, beta, gamma):
        self._b1 = 1 / (beta * delta_t**2)
    
    def update_b2(self, delta_t, beta, gamma):
        self._b2 = 1 / (beta * delta_t)
        
    def update_b3(self, delta_t, beta, gamma):
        self._b3 = -(1 / (2 * beta) - 1)
        
    def update_b4(self, delta_t, beta, gamma):
        self._b4 = gamma / (beta * delta_t)
    
    def update_b5(self, delta_t, beta, gamma):
        self._b5 = 1 - gamma / beta
        
    def update_b6(self, delta_t, beta, gamma):
        self._b6 = delta_t * (1 - gamma / (2 * beta))

        
    def beta(self, b):
        self.update_b1(self._delta_t, b, self._gamma)
        self.update_b2(self._delta_t, b, self._gamma)
        self.update_b3(self._delta_t, b, self._gamma)
    
    def gamma(self, g):
        self.update_b4(self._delta_t, self._beta, g)
        self.update_b5(self._delta_t, self._beta, g)
        self.update_b6(self._delta_t, self._beta, g)
    
    def delta_t(self, t):
        self.update_b1(t, self._beta, self._gamma)
        self.update_b2(t, self._beta, self._gamma)
        self.update_b4(t, self._beta, self._gamma)
        self.update_b6(t, self._beta, self._gamma)
    
    # def sec_K(self, assemble):